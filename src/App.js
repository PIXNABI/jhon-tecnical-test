import React, { Fragment, useState, useEffect } from "react";
import Scheduler from "./components/Scheduler";
import SchedulerData from "./components/SchedulerData";
import { db } from "./Firebase";

const App = () => {
  /**
   * No usar async await si no es necesario, y no combinar con una primesa si vas a usar en then
   */
  const [Data, setData] = useState([{}]);

  const checkedit = (taskId, completed) => {
    //Aqui se actualiza en firebase, y firebase cuando cambia actualiza la interfaz
    db.collection('contacts').doc(taskId).update({ completed })

    //setData(Data.map(x => (x.name === task.name ? {...x, completed: !x.completed} : x)))
  }

  const getDataInput = async (e) => {
    e.preventDefault()
    console.log(e.target);
    const date = new Date();
    const dateFull = `${date.getFullYear()}/${date.getMonth()}/${date.getDay()} ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`
    
    
    // No hay ningun procesimiento despues, esperando que esta linea se cumpla, por lo que el async await resulta innecesario.
    await db.collection('contacts').add({name: e.target.name.value, title: e.target.title.value, content: e.target.content.value, date: dateFull})
    .then((result) => {
      console.log("data send"); 
    }).catch((err) => {
      console.log(err);
    });

  }

  const deleteData = id => {
    const confirm = window.confirm('estas seguro que quieres elimiar esta tarea?');
    if (confirm) {
      db.collection('contacts').doc(id).delete()
    }
  }

  const editData = (id, Data) => {
    db.collection('contacts').doc(id).update(Data)
  }

  useEffect(() => {
    /**
     * 1: De esta manera se establece una conexion permanente con firebase
     * en cuanto firebase cambie, la data cambiará
     * 
     * 2: 
     */
    db.collection('contacts')
      .onSnapshot(snapShop => {
        const docs = snapShop.docs.map(doc => ({ ...doc.data(), id: doc.id }));
        setData(docs);
      }, error => {
        /*
        * Siempre que haya un error, se debe informar. Que quien usa la aplicacion
        * sepa que algo salio mal, y la cosas no funcionaran como espera.
        */
        alert('Error obtniendo la lista de tareas');
        console.error(error);
      })
  }, []);

  return (
    <Fragment>
      <div className="container">
        <Scheduler getDataInput={getDataInput}/>
        <SchedulerData Data={Data} checkedit={checkedit} deleteData={deleteData} editData={editData} />
      </div>
    </Fragment>
  );
};

export default App;
