import React from "react";

const Scheduler = prop => {
  return (
    <div className="form-container">
      <div className="card-form">
        <form onSubmit={prop.getDataInput}>
          <div>
            <input type="text" name="name" placeholder="Ingrese su nombre" />
          </div>
          <div>
            <input type="text" name="title" placeholder="Ingrese su Titulo" />
          </div>
          <div>
            <textarea
            name="content"
              placeholder="Ingrese su contenido"
            ></textarea>
          </div>
          <div>
            <input type="submit" value="Agregar a la lista" />
          </div>
        </form>
      </div>
    </div>
  );
};

export default Scheduler;
