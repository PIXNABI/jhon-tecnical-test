import React, { memo } from "react";

//Si las propiedades son pocas, se destructuran y se ahorra codigo dentro del render
const SchedulerData = ({ Data, checkedit, deleteData, editData }) => {
  /**
   * Nunca usar variables con nombres como `x`, las variables deben describir que son a la primera lectura de ser posible
   */
  return (
    <div className="results">
      <ul>
        {
          Data
            .filter(({ name }) => name && name !== '') //Este filter evita tener una condicion ternaria dentro del map y se entiende mejor
            .map(task => (
              <div key={task.id} className="result-container">
                <li>Nombre: {task.name}</li>
                <li>Titulo: {task.title}</li>
                <p>Contenido: {task.content}</p>
                <li>Fecha: {task.date}</li>
                <div>
                  <label>Marker</label>
                  <input
                    type="checkbox"
                    checked={task.completed}
                    onChange={(event) => checkedit(task.id, event.target.checked)}
                  />

                  <button>
                    <i className="fas fa-pen" onClick={ () => editData(task.id, task) }></i></button>
                  <button onClick={() => deleteData(task.id) }><i className="fas fa-trash" ></i></button>
                </div>
              </div>
          ))
        }
      </ul>
    </div>
  );
};

// Utiliza memo para evitar renderizados innecesarios que afecten el rendimiento
export default memo(SchedulerData);
